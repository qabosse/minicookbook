<?php

class Search {

    private $_title,        //string
             $_ingredients, //string
             $_needs_oven,  //string : 'true' or 'false'
             $_vege_status; //string : 'omni', 'vegetarian' or 'vegan'

    public static function attributeKeys()
    {
        return array("title","ingredients","needs_oven","vege_status");
    }

    public function exportArray()
    {
        return array_combine(Search::attributeKeys(), array($this->_title, $this->_ingredients, $this->_needs_oven, $this->_vege_status) );
    }

    public function setTitle($title)
    {
        $this->_title=htmlspecialchars($title);
    }

    public function setIngredients($ingredients)
    {
        $this->_ingredients=htmlspecialchars($ingredients);
    }

    public function setNeeds_oven($needs_oven)
    {
        ($needs_oven==='true' OR $needs_oven==='false') ? $this->_needs_oven = $needs_oven : $this->_needs_oven = null ; 
    }

    public function setVege_status($vege_status)
    {
        ($vege_status==='omni' OR $vege_status==='vegan' OR $vege_status==='vegetarian') ? $this->_vege_status= $vege_status : $this->_vege_status= null; 
    }

    public function __construct(Array $data)
    {
        $this->update($data);
    }

    public function update(Array $data)
        {
         foreach ($data as $key => $value)
          {
                $method = 'set'.ucfirst($key);
                if (method_exists($this, $method))
                {
                    $this->$method($value);
                }
          }
        }

    public function viewForm($target)
    {
        $vege_status_checked_status=[ ($this->_vege_status==='omni' ? 'checked' : ''), 
                                      ($this->_vege_status==='vegetarian' ? 'checked' : ''),
                                      ($this->_vege_status==='vegan' ? 'checked' : '')];
      
        $needs_oven_checked_status=[ ($this->_needs_oven===null ? 'checked' : ''),
                                     ($this->_needs_oven==='true' ? 'checked' : ''),  
                                     ($this->_needs_oven==='false' ? 'checked' : '')];
        $html_code= <<<EOT
        <form method="post" action="$target">
            <fieldset>
                <legend>Je cherche un plat...</legend>
                <input type="radio" name="vege_status" value="omni" {$vege_status_checked_status[0]} /> <label for="omni">Omni</label>
                <input type="radio" name="vege_status" value="vegetarian" {$vege_status_checked_status[1]}/> <label for="vegetarian">VG</label>
                <input type="radio" name="vege_status" value="vegan" {$vege_status_checked_status[2]}/> <label for="vegan">vegan (ou veganisable)</label>
            </fieldset>
            <fieldset>
                <legend>...qui contient...</legend>
                <label for="title">dans le titre : </label> <input type='text' name='title' id='title' value="{$this->_title}" /><br/>
                <label for="ingredients">dans les ingrédients :</label> <input size=60 type='text' name='ingredients' id='ingredients' value="{$this->_ingredients}"/>
             </fieldset>
            <fieldset>
                <legend>Recette au four ?</legend>
                <input type="radio" name="needs_oven" value=null {$needs_oven_checked_status[0]} /> <label for="dontcare">Je m'en fiche</label>
                <input type="radio" name="needs_oven" value="true" {$needs_oven_checked_status[1]} /> <label for="yes">Oui</label>
                <input type="radio" name="needs_oven" value="false" {$needs_oven_checked_status[2]} /> <label for="no">Non</label>
            </fieldset>
            <input type="submit" value="Rechercher"/>
        </form>
EOT;

        return $html_code;
    }
}
