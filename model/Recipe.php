<?php
class Recipe {
    private $_id,
    $_author,
    $_title,
    $_ingredients,      // array
    $_instructions,     // array
    $_is_vegan,         // 0 or 1
    $_is_vegetarian,    // 0 or 1
    $_needs_oven;       // 0 or 1

    public static function attributeKeys()
    {
        return array("author", "title", "ingredients", "instructions", "is_vegan", "is_vegetarian","needs_oven");
    }

    public function setId(int $id)
    {
        $this->_id=$id;
    }

    public function getId()
    {
        return $this->_id;
    }

    private function setAuthor($author)
    {
        $this->_author=htmlspecialchars($author);
    }

    public function getAuthor()
    {
        return $this->_author;
    }

    private function setTitle($title)
    {
        $this->_title=trim(htmlspecialchars($title));
    }

    public function getTitle()
    {
        return $this->_title;
    }

    private function setIngredients($ingredients)
    {
       if ((trim($ingredients) !== '') AND gettype($ingredients) == 'string') {
             $this->_ingredients=explode("\n",htmlspecialchars($ingredients));
        }
    }

    public function getIngredients()
    {
        if ($this->_ingredients !== null ) {
            return implode("\n",$this->_ingredients);
        }
    }

    private function setInstructions($instructions)
    {
        if ((trim($instructions) !== '') AND gettype($instructions) == 'string') {
            $this->_instructions=explode("\n",htmlspecialchars($instructions));
        }
    }

    public function getInstructions()
    {
        return implode("\n",$this->_instructions);
    }

    private function setIs_vegetarian($is_vegetarian)
    {
        if (gettype($is_vegetarian)!== 'null') {
            $this->_is_vegetarian=intval(intval($is_vegetarian)==1);
        }
    }

    public function getIs_vegetarian()
    {
        return $this->_is_vegetarian;
    }

    private function setIs_vegan($is_vegan)
    {
        if (gettype($is_vegan)!== 'null') {
            $this->_is_vegan=intval(intval($is_vegan)==1);
        }
    }

    public function getIs_vegan()
    {
        return $this->_is_vegan;
    }

    private function setNeeds_oven($needs_oven)
    {
        if (gettype($needs_oven)!== 'null') {
            $this->_needs_oven=intval(intval($needs_oven)==1);
        }
    }

    public function getNeeds_oven()
    {
        return $this->_needs_oven;
    }

    public function __construct(Array $parameters)
    {
        $this->edit($parameters);
    } 

/**
* This method checks whether the recipe contains a title and at least an ingredient or an instruction, and whether its vegetarian/vegan status is coherent
*
* @return bool
*/
    public function isValid()
    {
        return !(($this->_title=='') OR
        (($this->_ingredients=='') AND ($this->_instructions=='')) OR
        (($this->_is_vegan==1) AND ($this->_is_vegetarian==0)));       
    }

/**
* This method checks whether the recipe is saved in a given RecipeManager or not
*
* @param RecipeManager $recipe_db : the RecipeManager where the recipe is supposed to be stored.
*
* @return bool
*/
    public function isSavedIn(RecipeManager $recipe_db)
    {
        if (isset($this->_id) AND ($recipe_db->load($this->_id) !== null ))
        {
            return $this==$recipe_db->load($this->_id);
        }        
        else {
            return False;
        }
    }

    public function edit($parameters)
    {
         foreach ($parameters as $key => $value)
            {
                $method = 'set'.ucfirst($key);
                if (method_exists($this, $method))
                {
                  $this->$method($value);
                }
            }
        if ($this->_is_vegan == 1) {
        $this->setIs_vegetarian(1);
        }
    }

    public function viewHtml()
    {
        $info=array();
        if ($this->_needs_oven ==1){
            $info[]="avec four";
        }
        elseif ($this->_needs_oven==0){
            $info[]="sans four";
        }
        if ($this->_is_vegetarian==1 AND $this->_is_vegan==0){
            $info[]= "végétarien";
        }
        elseif($this->_is_vegan==1){
            $info[]="vegan (ou véganisable)";
        }
        if (count($info)>0){
            $info="<div class='info'>".ucfirst(implode(", ", $info))."</div>";
        }
        else {
            $info='';
        }
        $html_code= <<<EOT
            <article class='recipe'>
            <h1>  $this->_title </h1>
             $info
EOT;
        if (count($this->_ingredients)>0){
            $html_code.= <<<EOT
                <div class='ingredients'>
                    <h2> Ingrédients :</h2>
                        <ul>
EOT;
            foreach ($this->_ingredients as $ingredient) {
                $html_code.="\n <li> $ingredient </li>";
             }
            $html_code.="\n </ul> \n </div>";
        }
        if (count($this->_instructions)>0) {
            $html_code.=<<<EOT
                <div class='instructions'>
                    <h2>Indications :</h2>
EOT;
            foreach ($this->_instructions as $instruction) {
                $html_code.="\n <p> $instruction </p>";
            }
            $html_code.="\n </div>";
        }
        $html_code.=" \n </article>";

        return $html_code;
    }

    public function viewForm($target)
    {
        $is_vegetarian_checked_status=[ ($this->_is_vegetarian===1 ? 'checked' : ''),  ($this->_is_vegetarian===0 ? 'checked' : '')];
        $is_vegan_checked_status=[ ($this->_is_vegan===1 ? 'checked' : ''),  ($this->_is_vegan===0 ? 'checked' : '')];
        $needs_oven_checked_status=[ ($this->_needs_oven===1 ? 'checked' : ''),  ($this->_needs_oven===0 ? 'checked' : '')];

        $html_code= <<<EOT
            <form method="post" action="$target" >
                <fieldset>
                    <legend>Ma recette : </legend>
                    <p><label for="title">Titre :</label> <input size=60 type='text' name='title' id='title' value="{$this->getTitle()}" />  </p>
                    <p><label for="ingredients">Ingrédients :</label><br/>
                        <textarea name='ingredients' id='ingredients'>{$this->getIngredients()}</textarea>
                    </p>
                    <p><label for="instructions">Instructions:</label><br/>
                        <textarea name='instructions' id='instructions'>{$this->getInstructions()}</textarea>
                    </p>
                </fieldset>
                <fieldset>
                    <legend>Informations diverses :</legend>
                    <ul>
                        <li>
                            Cette recette est végétarienne :
                            <input type='radio' name='is_vegetarian' value="1" {$is_vegetarian_checked_status[0]} /><label for="1">Oui</label>
                            <input type='radio' name='is_vegetarian' value="0" {$is_vegetarian_checked_status[1]} /><label for="0">Non</label>
                        </li>
                        <li>
                            Cette recette est végane ou facilement véganisable :
                            <input type='radio' name='is_vegan' value="1" {$is_vegan_checked_status[0]} /><label for="1">Oui</label>
                            <input type='radio' name='is_vegan' value="0" {$is_vegan_checked_status[1]} /><label for="0">Non</label>
                        </li>
                        <li>
                            Cette recette nécessite un four :
                            <input type='radio' name='needs_oven' value="1" {$needs_oven_checked_status[0]} /><label for="1">Oui</label>
                            <input type='radio' name='needs_oven' value="0" {$needs_oven_checked_status[1]} /><label for="0">Non</label>
                        </li>
                    </ul>
                </fieldset>
                <input type="submit" value="Enregistrer"/>
            </form>
EOT;
        return $html_code;
    }
}
