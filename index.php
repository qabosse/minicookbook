<?php
require_once('model/Recipe.php');
require_once('model/RecipeManager.php');
require_once('model/Search.php');

$recipe_list=new RecipeManager(__DIR__);
$target='index.php';
$search=new Search($_POST);
$recipes=$recipe_list->search($search->exportArray());
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Carnet de recettes</title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="style.css" />
    </head>
    <body>


        <header>
            <h1>Navigation :</h1>
            <section id='search'>
                <?= $search->viewForm($target) ?>
            </section>
        </header>
        <h1>Résultats : </h1>
        <section id='results'>
            <?php
                foreach ($recipes as $recipe)
                {
                    echo $recipe->viewHtml();
                }
            ?>
        </section>
        <?php include("footer.php"); ?>
    </body>
</html>
